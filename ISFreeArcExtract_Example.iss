;����������� ������ ���������� FreeArc ������ ��� ������ unarc.dll, � ������������ ��������� ���������� � ���� Inno Setup � �������� ���������� �����.

#define ArcIni "{tmp}\arc.ini"

#include "ISFreeArcExtract_Core.iss"

[Setup]
AppName=FreeArc Example
AppVerName=FreeArc Example 4.1 Extreme
DefaultDirName={pf}\FreeArc Example
DirExistsWarning=no
ShowLanguageDialog=auto
OutputBaseFilename=FreeArc_Example-Ext
OutputDir=.
VersionInfoCopyright=Bulat Ziganshin, Victor Dobrov, SotM, CTACKo, Shegorat

[UninstallDelete]
Type: filesandordirs; Name: {app}

[Components]
Name: Russian; Description: ����������� ��������� � �������
Name: English; Description: �������� ������� �����; Types: compact full

[Languages]
Name: eng; MessagesFile: compiler:Default.isl
Name: rus; MessagesFile: compiler:Languages\Russian.isl
Name: ger; MessagesFile: compiler:Languages\German.isl

[CustomMessages]
eng.ArcBreak=Installation cancelled!
eng.ArcError=Decompression failed with error code %1
eng.ErrorUnknownError=Error occured while extracting archives. Please contact the author of this program.
eng.ErrorCompressMethod=Compression method "%1" of the acrhive "%2" is not supported by the current version of unarc.dll.
eng.ErrorOutBlockSize=Output block is too small for the archive "%1".
eng.ErrorNotEnoughRAMMemory=Not enough memory to unpack "%1" archive.
eng.ErrorReadData=Archive "%1" data read error.
eng.ErrorBadCompressedData=Data can't be extracted from the archive "%1".
eng.ErrorNotImplement=Required action isn't supported by the current version of unarc.dll.
eng.ErrorDataAlreadyDecompress=Data block of the archive "%1" is already decompressed.
eng.ErrorUnpackTerminated=User cancelled the operation.
eng.ErrorWriteData=Error occured while processing the archive "%1".
eng.ErrorBadCRC=Bad CRC in the archive "%1".
eng.ErrorBadPassword=Applied password for archive "%1" isn't correct.
eng.ErrorBadHeader=Header of the archive "%1" is corrupted.
eng.ErrorCodeException=Raised a code exception. Please contact the author of this program.
eng.ErrorNotEnoughFreeSpace=Not enough free space to unpack "%1".
eng.ArcTitle=Extracting FreeArc archives...
eng.StatusInfo=Files: %1%2, progress %3%%, remaining time %4
eng.ArcInfo=Disk %1 of %2, archive %3 of %4, %5%% of archive processed
eng.ArcFinish=Unpacked archives: %1, received files: %2 [%3]
eng.InsertDisk=Please insert disk � %1 with file "%2" and press OK.
eng.taskbar=%1%%, %2 remains
eng.ending=Ending
eng.unknown=Unknown
eng.hour= hours
eng.min= mins
eng.sec= secs
eng.BrowseTitle=Browse directories
eng.BrowseOK=OK
eng.BrowseCancel=Cancel
eng.BrowseError=The file with such filename does not exists in directory!

rus.ArcBreak=��������� ��������!
rus.ArcError=����������� FreeArc ������ ��� ������: %1.
rus.ErrorUnknownError=������ ��� ���������� �������. ����������, ���������� � ������������ ���������.
rus.ErrorCompressMethod=����� ������ "%1" ������� ������ "%2" �� �������������� ������ ������� unarc.dll.
rus.ErrorOutBlockSize=�������� ���� ������ ������ "%1" ������� ���.
rus.ErrorNotEnoughRAMMemory=������������ ��������� ����������� ������ ��� ���������� ������ "%1".
rus.ErrorReadData=������ ������ ������ ������ "%1".
rus.ErrorBadCompressedData=������ �� ������ "%1" �� ����� ���� �����������.
rus.ErrorNotImplement=����������� �������� �� �������������� ������ ������� unarc.dll.
rus.ErrorDataAlreadyDecompress=����������� ���� ������ ������ "%1" ��� ����������.
rus.ErrorUnpackTerminated=�������� �������� �������������.
rus.ErrorWriteData=������ ������ ������ �� ������ "%1".
rus.ErrorBadCRC=������ ������ CRC � ������ "%1".
rus.ErrorBadPassword=������ ��������� ��� ������� ������ "%1" �������.
rus.ErrorBadHeader=��������� ������ "%1" ���������.
rus.ErrorCodeException=������ ���������� ��������� ���������. ����������, ���������� � ������������ ���������.
rus.ErrorNotEnoughFreeSpace=������������ ���������� ����� �� ����� ���������� ��� ���������� ������ "%1".
rus.ArcTitle=���������� FreeArc-�������...
rus.StatusInfo=������: %1%2, %3%% ���������, �������� ����� %4
rus.ArcInfo=���� %1 �� %2, ����� %3 �� %4, ����� ��������� �� %5%%
rus.ArcFinish=����������� �������: %1, �������� ������: %2 [%3]
rus.InsertDisk=����������, �������� ���� � %1, ���������� ���� "%2" � ������� ������ ��.
rus.taskbar=%1%%, ��� %2
rus.ending=����������
rus.unknown=����������
rus.hour= �����
rus.min= ���
rus.sec= ���
rus.BrowseTitle=����� ����������
rus.BrowseOK=��
rus.BrowseCancel=������
rus.BrowseError=���� � �������� ������ �� ������ � ��������� �����!

ger.ArcBreak=Installation abgebrochen!
ger.ArcError=Entpacken brach mit dem Fehlercode %1 ab.
ger.ErrorUnknownError=Ein Fehler trat beim Entpacken auf. Kontaktieren sie den Autor des Programms.
ger.ErrorCompressMethod=Die Kompressions-Methode "%1" des Archivs "%2" wird von der aktuellen Version von unarc.dll nicht unterstutzt.
ger.ErrorOutBlockSize=Ausgabe-Block ist zu klein fur das Archiv "%1"
ger.ErrorNotEnoughRAMMemory=Nicht genug freier Speicher um das Archiv "%1" zu entpacken.
ger.ErrorReadData=Archiv "%1" Daten-Lese-Fehler
ger.ErrorBadCompressedData=Daten konnten nicht aus dem Archiv "%1" entpackt werden.
ger.ErrorNotImplement=Die benotigte Funktion wird noch nicht von unarc.dll unterstutzt.
ger.ErrorDataAlreadyDecompress=Daten-Block des Archivs "%1" ist bereits entpackt.
ger.ErrorUnpackTerminated=Abbruch durch Benutzer.
ger.ErrorWriteData=Ein Fehler ereignete sich beim Bearbeiten des Archivs "%1"
ger.ErrorBadCRC=Falsche CRC-Prufsumme in Archiv "%1"
ger.ErrorBadPassword=Passwort fur Archiv "%1" ist falsch.
ger.ErrorBadHeader=Header des Archivs "%1" ist beschadigt.
ger.ErrorCodeException=Code-Ausnahme erkannt. Kontaktieren sie den Autor des Programms.
ger.ErrorNotEnoughFreeSpace=Nicht genug freier Speicher um "%1" zu entpacken!
ger.ArcTitle=Entpacke FreeArc-Archive...
ger.StatusInfo=Dateien: %1%2, Fortschritt %3%%, verbleibende Zeit %4
ger.ArcInfo=Disk %1 von %2, Archiv %3 von %4, %5%% des Archivs bearbeitet.
ger.ArcFinish=Entpackte Archive: %1, erhaltene Dateien: %2 [%3]
ger.InsertDisk=Bitte legen Sie Disk # %1 mit der Datei "%2" ein und drucken Sie OK.
ger.taskbar=%1%%, %2 verbleibend
ger.ending=Fertigstellen
ger.unknown=Unbekannten
ger.hour= Stunden
ger.min= Minuten
ger.sec= Sekunden
ger.BrowseTitle=
ger.BrowseOK=
ger.BrowseCancel=
ger.BrowseError=


[Files]
Source: ISFAEFiles\unarc.dll; DestDir: {tmp}; Flags: dontcopy
Source: ISFAEFiles\CallBackCtrl.dll; DestDir: {tmp}; Flags: dontcopy


[ArcFiles]
;Source: {src}\*.bin; DestDir: {app}; Disk: 1;
Source: {src}\Data1.bin; DestDir: {app}\data;
Source: {src}\Data2.bin; DestDir: {app}\data; Disk: 1; Components: Russian; Config: {tmp}\arc1.ini
Source: {src}\Data3.bin; DestDir: {app}\data2; Disk: 1; Components: English; Config: {tmp}\arc2.ini
//Source: {src}\Data4.bin; DestDir: {app}\data3; Disk: 2;
//Source: {src}\Data5.bin; DestDir: {app}\data4; Disk: 2; Components: Russian;
//Source: {src}\Data6.bin; DestDir: {app}\data5; Disk: 2; Components: Russian;
{#ParseArcFiles}

//[ArcFiles] Section directives:
//
// Source     - Name of the archive  (Mandatory)
// DestDir    - Destination directory for unpacking archives (Mandatory)
// Components - Names of components that need for unpacking archive (Optional)
// Tasks      - Names of tasks that need for unpacking archive (Optional)
// Disk       - Number of the disk that contains the archive (Optional)
// Password   - Acrhive password, if archive is encrypted (Optional)
// Config     - Path to arc.ini file, overrides global ArcIni constant for current archive (Optional)
// Flags      - Various modificators (Reserved for future usage) (Optional) (not implemented)
//   * skipifdoesntexists - not implemented
//   * autodelete         - not implemented
//   * forceoverwrite     - not implemented
//   * ???

[Code]
var
  StatusLabel, FileNameLabel, ExtractFile, StatusInfo: TLabel;
  ProgressBar: TNewProgressBar; OutErroMsg, CurStage: String;
  UnPackError: Integer; ContinueInstall: Boolean;

Function CreateLabel(Parent: TWinControl; AutoSize, WordWrap, Transparent: Boolean; FontName: String; FontStyle: TFontStyles; FontColor: TColor; Left, Top, Width, Height: Integer; Prefs: TObject): TLabel;
Begin
  Result:= TLabel.Create(Parent);
  Result.Parent:= Parent;
  if Prefs <> Nil then begin
    Top:= TWinControl(Prefs).Top;
    Left:= TWinControl(Prefs).Left;
    Width:= TWinControl(Prefs).Width;
    Height:= TWinControl(Prefs).Height;
  end;

  if Top > 0 then
    Result.Top:=Top;
  if Left > 0 then
    Result.Left:= Left;
  if Width > 0 then
    Result.Width:= Width;
  if Height > 0 then
    Result.Height:= Height;

  if FontName <> '' then
    Result.Font.Name:= FontName;
  if FontColor > 0 then
    result.Font.Color:= FontColor;
  if FontStyle <> [] then
    result.Font.Style:= FontStyle;

  Result.AutoSize:= AutoSize;
  result.WordWrap:= WordWrap;
  result.Transparent:= Transparent;
  result.ShowHint:= True;
End;

Function NumToStr(Float: Extended): String;
Begin
  Result:= Format('%.2n', [Float]); StringChange(Result, ',', '.');
  while ((Result[Length(Result)] = '0') or (Result[Length(Result)] = '.')) and (Pos('.', Result) > 0) do
    SetLength(Result, Length(Result)-1);
End;

Function ByteOrTB(Bytes: Extended; noMB: Boolean): String;
Begin
  if not noMB then Result:= NumToStr(Int(Bytes)) +' Mb' else
    if Bytes < 1024 then if Bytes = 0 then Result:= '0' else Result:= NumToStr(Int(Bytes)) +' Bt' else
      if Bytes/1024 < 1024 then Result:= NumToStr(round((Bytes/1024)*10)/10) +' Kb' else
        If Bytes/oneMB < 1024 then Result:= NumToStr(round(Bytes/oneMB*100)/100) +' Mb' else
          If Bytes/oneMB/1000 < 1024 then Result:= NumToStr(round(Bytes/oneMB/1024*1000)/1000) +' Gb' else
            Result:= NumToStr(round(Bytes/oneMB/oneMB*1000)/1000) +' Tb';
End;

function UpdateProgress(StatusText, FilenameText, Time, ErrorMessage: String; PositionTotal, MaxTotal, PositionCurrent, MaxCurrent, FileCount, CurDisk, DiskCount: Integer; ExtractedSize: Extended): Boolean;
var
  totalP, arcP: Extended;
begin
  if ProgressBar.Max<>MaxCurrent then
    ProgressBar.Max:= MaxCurrent;

  if WizardForm.ProgressGauge.Max<>MaxTotal then
    WizardForm.ProgressGauge.Max:= MaxTotal;

  ProgressBar.Position:= PositionCurrent;
  WizardForm.ProgressGauge.Position:= PositionTotal;

  if (MaxTotal <> 0) then
    totalP:= Extended(PositionTotal)*100/Extended(MaxTotal);
  if (MaxCurrent <> 0) then
    arcP:= Extended(PositionCurrent)*100/Extended(MaxCurrent);

  StatusLabel.Caption:= StatusText;
  OutErroMsg:= ErrorMessage;
  FilenameLabel.Caption:= FilenameText;
  CurStage:= StatusText;
  StatusInfo.Caption:= FmtMessage(cm('StatusInfo'), [IntToStr(FileCount), ' ['+ ByteOrTB(ExtractedSize, true) +']', Format('%.1n', [Abs(totalP)]), Time]);
  ExtractFile.Caption:= FmtMessage(cm('ArcInfo'), [IntToStr(CurDisk), IntToStr(DiskCount), IntToStr(ArcInd+1), IntToStr(GetArrayLength(Arcs)), Format('%.1n', [Abs(arcP)])]);
  ProgressBar.Position:= LastMb;
  Result:= ContinueInstall;
end;

function BeforeExtract(): Boolean;
begin
  // ����� ����� ������� ����������� ����� ����� �����������
  Result:= True;
end;

procedure CurStepChanged(CurStep: TSetupStep);
var n: Integer;
begin
  if CurStep = ssPostInstall then
  begin
    ContinueInstall:= True;

    WizardForm.CancelButton.Enabled:= True;

    ProgressBar.Position:=0;
    WizardForm.ProgressGauge.Position:= 0;

    StatusLabel.Show;
    FileNameLabel.Show;
    StatusInfo.Show;
    ProgressBar.Show;
    ExtractFile.Show;

    UnPackError:= ISFAExtract('{#Archives}', @BeforeExtract, @UpdateProgress);

    if UnPackError <> 0 then begin 
      Exec(ExpandConstant('{uninstallexe}'), '/SILENT','', sw_Hide, ewWaitUntilTerminated, n);
      WizardForm.caption:= SetupMessage(msgErrorTitle) +' - '+ cm('ArcBreak');
      SetTaskBarTitle(SetupMessage(msgErrorTitle));
    end else
      SetTaskBarTitle(SetupMessage(msgSetupAppTitle));

    StatusLabel.Hide;
    FileNameLabel.Hide;
    StatusInfo.Hide;
    ProgressBar.Hide;
    ExtractFile.Hide;
  end;
end;

Procedure CurPageChanged(CurPageID: Integer);
Begin
  if (CurPageID = wpFinished) and (UnPackError <> 0) then begin
    WizardForm.FinishedLabel.Font.Color:= $0000C0;
    WizardForm.FinishedLabel.Height:= WizardForm.FinishedLabel.Height * 2;
    WizardForm.FinishedLabel.Caption:= SetupMessage(msgSetupAborted) + #13#10#13#10 + OutErroMsg;
  end;
End;

procedure WizardClose(Sender: TObject; var Action: TCloseAction);
Begin
  Action:= caNone; 
  if CurStage = cm('ArcTitle') then begin
    if MsgBox(SetupMessage(msgExitSetupMessage), mbInformation, MB_YESNO) = IDYES then
      ContinueInstall:= false;
  end else
    MainForm.Close;
End;

Procedure InitializeWizard();
Begin
  Log('{#Archives}');

  StatusLabel:= CreateLabel(WizardForm.InstallingPage,false,false,true,'',[],0,0,0,0,0, WizardForm.StatusLabel);
  FileNameLabel:= CreateLabel(WizardForm.InstallingPage,false,false,true,'',[],0,0,0,0,0, WizardForm.FileNameLabel);
  WizardForm.StatusLabel.Top:= WizardForm.ProgressGauge.Top; WizardForm.FileNameLabel.Top:= WizardForm.ProgressGauge.Top;    // ������ ��� �����������, ����� ��� ������� WM_PAINT ���������������

  with WizardForm.ProgressGauge do begin
    StatusInfo:= CreateLabel(WizardForm.InstallingPage, false, true, true, '', [], 0, 0, Top + ScaleY(32), Width, 0, Nil);
    ProgressBar := TNewProgressBar.Create(WizardForm);
    ProgressBar.SetBounds(Left, StatusInfo.Top + StatusInfo.Height + ScaleY(16), Width, Height);
    ProgressBar.Parent := WizardForm.InstallingPage;
    ProgressBar.Max := 65536;
    ProgressBar.Hide;
    ExtractFile:= CreateLabel(WizardForm.InstallingPage, false, true, true, '', [], 0, 0, ProgressBar.Top + ScaleY(32), Width, 0, Nil);
    StatusLabel.Hide;
    FileNameLabel.Hide;
    StatusInfo.Hide;
    ProgressBar.Hide;
    ExtractFile.Hide;
  end;

  WizardForm.OnClose:= @WizardClose
End;


