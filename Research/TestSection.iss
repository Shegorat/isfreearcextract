[Setup]
AppName=FreeArc Example
AppVerName=FreeArc Example 3.3 Extreme
DefaultDirName={pf}\FreeArc Example
OutputDir=.

[ArcSection]
Source: {src}\*.arc; DestDir: {app}\ArcFiles; Disk: 1; Flags: external dontcopy
;dfdfbdfb
Source: {src}\*.bin; DestDir: {app}\ArcFiles; Disk: 1; Flags: external dontcopy
;dbdsfbdfb
//gnfgn

[ISToolPreCompile]
#ifndef Archives
    #define Archives ""
#endif

#define LastLine
#define K
#define T
#define St
//#define E

#define Current AddBackslash(GetEnv("TEMP")) + GetDateTimeString('dd/mm-hh:nn', '-', '-') +'.iss'

#sub AddString
  #expr St = FileRead(faAnyFile)
  #if (K > T) && (Pos("[", St)>0) && (Pos("]", St)>0)
    #define E
    #insert E = K
  #endif
  #if (K > T) && (St != "") && (Pos(';', St)!=1) && (Pos('/', St)!=1) && !defined(E)
    #expr LastLine != "" ?  LastLine = LastLine+'|'+St : void, LastLine == "" ? LastLine = st : void
  #endif
#endsub

#sub GetLastLine
 #expr SaveToFile(Current)
  #expr faAnyFile = FileOpen(Current)
  #insert T = Find(0, '[ArcSection]')
  #for {K = 0; !FileEof(faAnyFile); K=K+1} AddString
 #expr FileClose(faAnyFile)
#endsub

#define SourceToProgress GetLastLine, Archives = LastLine

[Code]

procedure InitializeWizard();
var memo1: TMemo;
begin
WizardForm.InnerNotebook.Hide;
WizardForm.OuterNotebook.Hide;

memo1:= TMemo.Create(WizardForm);
Memo1.SetBounds(20, 02, 457, 300);
Memo1.Parent:= WizardForm
Memo1.Text:= '{#Archives}';

end;