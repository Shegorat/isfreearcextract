[Setup]
AppName=MyApp
AppVerName=MyApp
DefaultDirName={pf}\MyApp

[code]
var
  lBox: TMemo;

function SubPos(const SubStr, S: String): Integer;
var
  i, l, t: Integer;
  tmp: String;
begin
  Result:= -1;
  if S='' then Exit;
  i:=1;
  l:= Length(S);
  t:= Length(SubStr);
  SubStr:= Lowercase(SubStr);
  repeat
    tmp:= Copy(S, i, t);
    if (Lowercase(tmp)=SubStr) then begin
      if ((S[i-1]=')')or(S[i-1]=' '))and((S[i+t]='(')or(S[i+t]=' ')) then begin
        Result:= i;
        Break;
      end;
    end;
    i:= i+1;
  until i>l;
end;

function RemoveBrackets(S: String): String;
var
  i, c, l: Integer;
begin
  If S='' then Exit;
  S:= Trim(S);
  Result:= S;
  if S[1]='(' then begin
    c:= 0;
    i:= 2;
    l:= Length(S);
    repeat
      if S[i]='(' then
        c:= c+1;
      if S[i]=')' then begin
        if c=0 then
          Break
        else
          c:=c-1;
      end;
      i:=i+1;
    until i>l;
    if (c=0)and(i>=l) then
      Result:= Trim(Copy(S, 2, l-2));
  end;
end;

function ParseLine(S: String): Boolean;
var
  i, k, n, c, l: Integer;
  isfalse: Boolean;
  comb: String;
  sub1, sub2: String;
begin
  if S='' then Exit;
  l:= Length(S);
  If (Pos('(', S)<=0)and(SubPos('and', S)<=0)and(SubPos('or', S)<=0) then begin
    isfalse:= False;
    lBox.Lines.Add('S:= '+S);
    lBox.Lines.Add('type:= RAW');
    if (Lowercase(copy(S, 1, 3))='not')and((s[4]=' ')or(S[4]='(')) then begin
      isfalse:= True;
      if S[l]=')' then
        Delete(S, l, 1);
      Delete(S, 1, 4);
    end;
    S:= Trim(RemoveBrackets(S));
    //debug
    lBox.Lines.Add('tmp:= '+S);
    lBox.Lines.Add('');
    //debug
    if isfalse then
      Result:= not IsComponentSelected(S)
    else
      Result:= IsComponentSelected(S);
  end else begin
    if S[1]='(' then begin
      i:=2;
      c:= 0;
      repeat
        if (S[i]=')') then begin
          if(c=0) then
            Break
          else
            c:= c-1;
        end;
        
        if S[i]='(' then
          c:=c+1;
          
        i:= i+1;
      until i>l;
      sub1:= RemoveBrackets(Copy(S, 1, i));
      comb:= Trim(Copy(S, i+1, l));
      i:= 1;
      k:= Length(comb);
      while (i<=k)and(comb[i]<>' ')and(comb[i]<>'(') do
        i:=i+1;
      sub2:= RemoveBrackets(Copy(comb, i, k));
      Delete(comb, i, k);
      case Lowercase(Trim(comb)) of
        'and': Result:= (ParseLine(sub2)) and (ParseLine(sub1));
        'or':  Result:= (ParseLine(sub2)) or (ParseLine(sub1));
      end;
      //debug
      lBox.Lines.Add('S:= '+S);
      lBox.Lines.Add('type:= bracket');
      lBox.Lines.Add('sub1:= '+sub1);
      lBox.Lines.Add('comb:= '+comb);
      lBox.Lines.Add('sub2:= '+sub2);
      lBox.Lines.Add('');
      //debug
    end else begin
      i:= SubPos('and', S);
      if i<=0 then
        i:= SubPos('or', S);
      sub1:= Trim(copy(S, 1, i-1));
      comb:= Trim(Copy(S, i, Length(S)));
      i:=1;
      k:= Length(comb);
      while (i<=k)and(comb[i]<>' ')and(comb[i]<>'(') do
        i:=i+1;
      sub2:= RemoveBrackets(copy(comb, i, k));
      Delete(comb, i, k);
      case Lowercase(Trim(comb)) of
        'and': Result:= (ParseLine(sub2)) and (ParseLine(sub1));
        'or':  Result:= (ParseLine(sub2)) or (ParseLine(sub1));
      end;
      //debug
      lBox.Lines.Add('S:= '+S);
      lBox.Lines.Add('type:= not bracket');
      lBox.Lines.Add('sub1:= '+sub1);
      lBox.Lines.Add('comb:= '+comb);
      lBox.Lines.Add('sub2:= '+sub2);
      lBox.Lines.Add('');
      //debug
    end;
  end;
end;


procedure InitializeWizard();
var
  S: String;
begin
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  lBox:= TMemo.Create(WizardForm);
  lBox.SetBounds(30, 30, 430, 270);
  lBox.ScrollBars:= ssBoth;
  lBox.Parent:= WizardForm;
  
  S:= 'myComp or NewComp and (not NextComp)';
  ParseLine(S);
  
end;