[Setup]
AppName=MyApp
AppVerName=MyApp
DefaultDirName={pf}\MyApp

[code]
var
  Browse_Form: TForm;
  Browse_DirEdit: TEdit;
  Browse_DirBox: TFolderTreeView;
  Browse_Name: String;
  
procedure OKOnClick(Sender: TObject);
var
  FSR: TFindRec;
begin
  if not FindFirst(Browse_DirBox.Directory+'\'+Browse_Name, FSR) then begin
    MsgBox('���� � �������� ������ �� ������ � ��������� �����!', mbError, MB_OK);
    Exit;
  end;
  FindClose(FSR);
  TButton(Sender).ModalResult:= mrOk;
end;

procedure DirBoxOnChange(Sender: TObject);
begin
  Browse_DirEdit.Text:= Browse_DirBox.Directory;
end;

procedure NewFolderOnClick(Sender: TObject);
begin
  Browse_DirBox.CreateNewDirectory('����� �����');
end;

function BrowseForFiles(const SrcPath, Filename: String; var OutPath: String): Boolean;
begin
  Browse_Form:= TForm.Create(MainForm);
  Browse_Form.ClientWidth:= 280;
  Browse_Form.ClientHeight:= 225;
  Browse_Form.BorderStyle:= bsSingle;
  Browse_Form.BorderIcons:= [biSystemMenu];
  Browse_Form.Caption:= '����� ����������';
  Browse_Form.Position:= poScreenCenter;

  Browse_Name:= Filename;
  
  with TLabel.Create(Browse_Form) do begin
    Autosize:= False;
    SetBounds(10, 8, 260, 20);
    Transparent:= True;
    Caption:= '����� � �������:';
    Parent:= Browse_Form;
  end;
  
  Browse_DirEdit:= TEdit.Create(Browse_Form);
  Browse_DirEdit.SetBounds(10, 25, 270, 20);
  Browse_DirEdit.Text:= SrcPath;
  Browse_DirEdit.Parent:= Browse_Form;
  
  Browse_DirBox:= TFolderTreeView.Create(Browse_Form);
  Browse_DirBox.SetBounds(10, 55, 270, 140);
  Browse_DirBox.Parent:= Browse_Form;
  Browse_DirBox.OnChange:= @DirBoxOnChange;
  Browse_DirBox.Directory:= SrcPath;

  with TButton.Create(Browse_Form) do begin
    SetBounds(135, 205, 70, 23);
    Caption:= '��';
    OnClick:= @OKOnClick;
    Parent:= Browse_Form;
  end;
  
  with TButton.Create(Browse_Form) do begin
    ModalResult:= mrCancel;
    SetBounds(210, 205, 70, 23);
    Caption:= '������';
    Parent:= Browse_Form;
  end;
  
  with TButton.Create(Browse_Form) do begin
    SetBounds(10, 205, 90, 23);
    Caption:= '����� �����';
    OnClick:= @NewFolderOnClick;
    Parent:= Browse_Form;
  end;
  
  if Browse_Form.ShowModal = mrCancel then
    Result:= False;

  OutPath:= Browse_DirBox.Directory;
  
  Browse_Form.Free;
end;


procedure InitializeWizard();
var
  S: String;
begin
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  BrowseForFiles('D:\', '', S);
end;